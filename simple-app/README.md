# simple app
Example simple app.

## build docker container
docker build -t registry.gitlab.com/pavliki/k3s-example:latest .

## docker login
docker login -u username -p password registry.gitlab.com

## push docker into repository
docker push registry.gitlab.com/pavliki/k3s-example:latest
