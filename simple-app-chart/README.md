# helm chart chart-app
Example helm chart for simple-app.

## create empty chart:
helm create simple-app

You can download prepared chart and use it.

## define docker-registry in your chart
imagePullSecrets:
  - name: gitlab-registry 
  
## deploy application into cluster

### check contexts
kubectl config get-contexts

### use context
kubectl config use-context default

### deploy application version 1.0.0
helm upgrade --install simple-app ./simple-app-chart/ -n app --create-namespace -f ./simple-app-chart/values.yaml --set "containers[0].image.tag=1.0.0"

### deploy application version latest
helm upgrade --install simple-app ./simple-app-chart/ -n app --create-namespace -f ./simple-app-chart/values.yaml

Version is used from values.yaml file.

## remove application from cluster
helm uninstall simple-app