# k3s example
Example k3s kubernetes cluster with 1 master node and 1 worker.

## run k3s cluster
set K3S_TOKEN

docker-compose -f k3s.yml up -d

## volumes
- .:/output (get kubeconfig.yaml from kubernetes)

## ports
- 6443:6443   # Kubernetes API Server
- 80:80       # Ingress controller port 80
- 443:443     # Ingress controller port 443
- 31111:31111 # simple-app

## save config
Get kubeconfig.yaml and set kubectl

### check contexts
kubectl config get-contexts

### use context
kubectl config use-context default

## create namespace
kubectl create namespace app

## set docker-registry named gitlab-registry

### create deploy token
https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html

### use deploy token
```
kubectl create secret docker-registry gitlab-registry -n app \
--docker-server=registry.gitlab.com \
--docker-username=username \
--docker-password=password
```
##docker-compose file source:
https://github.com/k3s-io/k3s/blob/v1.27.2%2Bk3s1/docker-compose.yml