# Example simple k3s cluster

## create application
see simple-app/README

## prepare k3s cluster
see k3s/README

## deploy application into k3s cluster
see simple-app-chart/README
